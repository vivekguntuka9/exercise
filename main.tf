module "route53" {
  source = "./modules/route53"
}

module "s3" {
  source = "./modules/s3"
}

module "cloudfront" {
  source = "./modules/cloudfront"
  s3_website_endpoint = module.s3.website_endpoint
}

module "lightsail" {
  source = "./modules/lightsail"
}

module "iam" {
  source = "./modules/iam"
}
