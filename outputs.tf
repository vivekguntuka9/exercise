output "cloudfront_domain_name" {
  value = module.cloudfront.cloudfront_domain_name
}

output "lightsail_instance_public_ip" {
  value = module.lightsail.instance_public_ip
}
