# Exercise / PoC
The Application Engineering team has been developing a custom WooCommerce-based product which will need to be deployed for this initiative. As a member of the DevOps Engineering team your job will be to create the cloud-based infrastructure for supporting this deployment. You will need to create a reference architecture and implement it using modern IaC techniques with documentation for 3-tier application. You can use serverless or container technology or VMs to implement this.

## Reference architecture 
The reference architecture is taken from [Best Practices for WordPress on AWS](https://docs.aws.amazon.com/pdfs/whitepapers/latest/best-practices-wordpress/best-practices-wordpress.pdf#welcome). 

## Architecture 
![alt text](./images/image.png)

### Note
Given the time constraints and the project's current phase as per description, I've opted for a Lightsail approach instead. It's important to note that this architecture isn't intended for production use. However, my Infrastructure as Code (IaC) adheres to production-grade standards through modularization.

## Further enhancemnets
There are a lot of improvements that are required to bring this to production grade. Below are few recommendations from me.
- The current CICD pipeline can be further strengthened by incorporating `linting, static application security testing (SAST), secret scanning, and infrastructure drift detection` capabilities.
- Implement `error handling mechanisms` to gracefully handle failures during Terraform execution. This could include conditional logic, error recovery strategies, or custom error messages.
- Consider using `remote backend` state for the collaborative terraform deployments.

## Terraform AWS Infrastructure

This Terraform configuration sets up a basic infrastructure on AWS, including Route 53, CloudFront, S3, Lightsail, and IAM resources.

## Prerequisites

Before you begin, ensure you have the following prerequisites installed:

- [Terraform](https://www.terraform.io/downloads.html) (version >= 1.2.0)
- [AWS CLI](https://aws.amazon.com/cli/) configured with appropriate credentials

## Getting Started

1. **Clone this repository:**

```bash
git clone <repository_url>
cd Exercise
```

2. **Initialize Terraform:**

```bash
terraform init
```

3. **Review and update variables** in `variables.tf` according to your requirements.

4. **Review and update the configuration** in each module as needed:
   - `modules/route53/main.tf`
   - `modules/s3/main.tf`
   - `modules/cloudfront/main.tf`
   - `modules/lightsail/main.tf`
   - `modules/iam/main.tf`

5. **Apply the Terraform configuration** to create the infrastructure:

```bash
terraform apply
```

6. After applying the configuration, you'll see the output variables containing relevant information about the created resources.

## Terraform Modules

This Terraform configuration is modularized to manage different components of the infrastructure separately:

- **Route 53 Module**: Manages Route 53 hosted zone.
- **S3 Module**: Manages S3 bucket for static content.
- **CloudFront Module**: Manages CloudFront distribution for caching static content.
- **Lightsail Module**: Manages Lightsail instance for dynamic content.
- **IAM Module**: Manages IAM role for Lightsail instance.

You can find the configuration files for each module in the `modules/` directory.

## Cleanup

To destroy the infrastructure and clean up resources, run:

```bash
terraform destroy
```

**Note:** This will permanently delete all resources created by Terraform.

