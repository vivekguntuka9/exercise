variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "domain_name" {
  description = "Domain name for Route 53 hosted zone"
  type        = string
  default     = "example.com"
}

variable "static_bucket_name" {
  description = "Name for the S3 bucket for static content"
  type        = string
  default     = "wordpressapp-static-bucket"
}

variable "index_document" {
  description = "Index document for the S3 bucket"
  type        = string
  default     = "index.html"
}

variable "error_document" {
  description = "Error document for the S3 bucket"
  type        = string
  default     = "error.html"
}

variable "min_ttl" {
  description = "Minimum TTL for CloudFront cache"
  type        = number
  default     = 0
}

variable "default_ttl" {
  description = "Default TTL for CloudFront cache"
  type        = number
  default     = 3600
}

variable "max_ttl" {
  description = "Maximum TTL for CloudFront cache"
  type        = number
  default     = 86400
}

variable "instance_name" {
  description = "Name for the Lightsail instance"
  type        = string
  default     = "wordpressapp-instance"
}

variable "blueprint_id" {
  description = "Blueprint ID for the Lightsail instance"
  type        = string
  default     = "wordpress"
}

variable "bundle_id" {
  description = "Bundle ID for the Lightsail instance"
  type        = string
  default     = "nano_2_0"
}

variable "availability_zone" {
  description = "Availability zone for the Lightsail instance"
  type        = string
  default     = "us-east-1a"
}
