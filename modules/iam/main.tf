resource "aws_iam_role" "wordpressapp_role" {
  name               = "wordpressapp-role"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Principal = {
          Service = "lightsail.amazonaws.com"
        },
        Action    = "sts:AssumeRole"
      }
    ]
  })
}
