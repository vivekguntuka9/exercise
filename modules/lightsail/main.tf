resource "aws_lightsail_instance" "wordpressapp_instance" {
  name              = var.instance_name
  blueprint_id      = var.blueprint_id
  bundle_id         = var.bundle_id
  availability_zone = var.availability_zone


  tags = {
    Name = "WordPressApp Instance"
  }
}
