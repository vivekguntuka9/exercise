variable "instance_name" {
  description = "Name for the Lightsail instance"
  type        = string
  default     = "wordpressapp-instance" 
}

variable "blueprint_id" {
  description = "Blueprint ID for the Lightsail instance"
  type        = string
  default     = "wordpressapp" 
}

variable "bundle_id" {
  description = "Bundle ID for the Lightsail instance"
  type        = string
  default     = "nano_2_0" 
}

variable "availability_zone" {
  description = "Availability zone for the Lightsail instance"
  type        = string
  default     = "us-east-1a" 
}
