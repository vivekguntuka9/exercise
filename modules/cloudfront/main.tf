resource "aws_cloudfront_distribution" "wordpressapp_distribution" {
  origin {
    domain_name = var.s3_website_endpoint
    origin_id   = "S3Origin"
  }

  enabled             = true
  default_root_object = var.index_document

  default_cache_behavior {
    target_origin_id = "S3Origin"

    viewer_protocol_policy = "redirect-to-https"
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD", "OPTIONS"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl             = var.min_ttl
    default_ttl         = var.default_ttl
    max_ttl             = var.max_ttl
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  tags = {
    Name = "WordPressApp CloudFront Distribution"
  }
}
