variable "min_ttl" {
  description = "Minimum TTL for CloudFront cache"
  type        = number
  default     = 0
}

variable "default_ttl" {
  description = "Default TTL for CloudFront cache"
  type        = number
  default     = 3600
}

variable "max_ttl" {
  description = "Maximum TTL for CloudFront cache"
  type        = number
  default     = 86400
}

variable "index_document" {
  description = "Index document for the CloudFront distribution"
  type        = string
  default     = "index.html" 
}

variable "s3_website_endpoint" {
  description = "Website endpoint of the S3 bucket"
  type        = string
}
