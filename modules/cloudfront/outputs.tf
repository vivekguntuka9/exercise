output "cloudfront_domain_name" {
  value = aws_cloudfront_distribution.wordpressapp_distribution.domain_name
}
