variable "domain_name" {
  description = "Domain name for Route 53 hosted zone"
  type        = string
  default     = "wordpressapp.com" 
}
