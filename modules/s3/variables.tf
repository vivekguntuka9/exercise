variable "static_bucket_name" {
  description = "Name for the S3 bucket for static content"
  type        = string
  default     = "wordpressapp-bucket" 
}

variable "index_document" {
  description = "Index document for the S3 bucket"
  type        = string
  default     = "index.html" 
}

variable "error_document" {
  description = "Error document for the S3 bucket"
  type        = string
  default     = "error.html" 
}
