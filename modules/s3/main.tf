
resource "aws_s3_bucket_acl" "static_bucket_acl" {
  bucket = aws_s3_bucket_website_configuration.static_bucket.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "static_bucket" {
  bucket = var.static_bucket_name

  index_document {
    suffix =  var.index_document
  }

  error_document {
    key = var.error_document
  }

}